/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */
const express = require('express')

const router = express.Router()
router.get('/', (req, res, next) => {
  res.render('index.ejs', { title: 'Express App' })
})

// Defer path requests to a particular controller
router.use('/admin', require('../controllers/admin.js'))
router.use('/user', require('../controllers/user.js'))
router.use('/intro', require('../controllers/intro'))

module.exports = router;